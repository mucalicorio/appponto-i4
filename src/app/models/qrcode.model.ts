export class Qrcode {
    qrcode: string;
    latitude: string;
    longitude: string;
    empresa: string;
}
