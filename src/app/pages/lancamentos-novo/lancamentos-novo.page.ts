import { Component, OnInit } from '@angular/core';
import { WarningService } from 'src/app/services/warning/warning.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { CompanyService } from 'src/app/services/company/company.service';
import { QrcodeService } from 'src/app/services/qrcode/qrcode.service';
import { EntriesService } from 'src/app/services/entries/entries.service';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-lancamentos-novo',
  templateUrl: './lancamentos-novo.page.html',
  styleUrls: ['./lancamentos-novo.page.scss'],
})
export class LancamentosNovoPage implements OnInit {

  btnEnabled: any;

  company: any;
  companies: any;
  qrcodes: any;
  resultValidate: any;

  lat: any;
  long: any;

  idUser: any;

  constructor(
    private scanner: BarcodeScanner,
    private warningService: WarningService,
    private companyService: CompanyService,
    private qrcodeService: QrcodeService,
    private entriesService: EntriesService,
    private userService: UserService,
    private router: Router,
    private geolocation: Geolocation
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.geolocation.getCurrentPosition()
      .then((data: any) => {
        this.lat = data.coords.latitude;
        this.long = data.coords.longitude;
      })
      .catch((err: any) => {
        console.log('Error: ', err);
      });

    this.btnEnabled = true;
    this.company = null;

    this.userService.getUserLogged()
      .then((userId) => {
        this.idUser = userId;
      });

    this.companyService.getCompanies()
      .then((companies) => {
        this.companies = JSON.parse(companies);
      })
      .catch((err) => {
        this.warningService.handleError('Erro ao carregar empresas.', err);
      });

    this.qrcodeService.getQrcodes()
      .then((qrcodes) => {
        this.qrcodes = JSON.parse(qrcodes);
      })
      .catch((err) => {
        this.warningService.handleError('Erro ao carregar qrcodes.', err);
      });
  }

  qrCodeReader() {
    this.scanner.scan()
      .then((data: any) => {
        this.validQrcode(data.text);
        if (this.resultValidate) {
          this.company = this.resultValidate.companyName;
        } else {
          this.warningService.showToast('QRCode não encontrado');
        }
      })
      .catch((err: any) => {
        this.warningService.handleError('Error: ', err);
      });
  }

  sendEntry() {
    const distLat = Math.sqrt(Math.pow((this.resultValidate.qrcodeLat - this.lat) * 111120.12, 2));
    const distLong = Math.sqrt(Math.pow((this.resultValidate.qrcodeLong - this.long) * 111120.12, 2));
    // const dist = Math.sqrt(Math.pow(distLat, 2) + Math.pow(distLong, 2));

    if (distLat <= 1000 && distLong <= 1000) {
      this.entriesService.getEntry()
        .then((entry) => {
          console.log('Entry on SendEntry: ', entry);
          if (entry) {
            this.sendExit();
          } else {
            this.sendEnter();
          }
        })
        .catch((err) => {
          this.warningService.handleError('Falha ao tentar pegar lançamento', err);
        });
    } else {
      this.warningService.showToast('Você está muito distante do ponto!');
    }

  }

  sendEnter() {
    console.log('Sending Enter');
    const datetimeEntry = new Date().toISOString();
    const entry = {
      id_qr_code: this.resultValidate.id_qr_code,
      id_usuario: this.idUser,
      entrada: datetimeEntry
    };
    this.entriesService.setEntry(JSON.stringify(entry));

    this.router.navigateByUrl('lancamentos');
  }

  sendExit() {
    console.log('Sending Exit');
    this.entriesService.getEntry()
      .then((entry: any) => {
        entry = JSON.parse(entry);
        entry.saida = new Date().toISOString();
        console.log('Entry to Exit: ', entry);
        this.entriesService.setEntry(JSON.stringify(entry));

        this.router.navigateByUrl('lancamentos');
      })
      .catch((err) => {
        this.warningService.handleError('Falha ao pegar lançamento.', err);
      });
  }

  async validQrcode(toValidate: string) {
    await this.qrcodes.forEach(qrcode => {
      if (toValidate === qrcode.qrcode) {
        this.companies.forEach(company => {
          if (qrcode.id_empresa === company.id_empresa) {
            this.resultValidate = {
              companyName: company.nome,
              qrcodeLat: qrcode.latitude,
              qrcodeLong: qrcode.longitude,
              id_qr_code: qrcode.id_qr_code
            };
          }
        });
      }
    });
  }

}
