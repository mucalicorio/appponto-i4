import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrcodesNovoPage } from './qrcodes-novo.page';

describe('QrcodesNovoPage', () => {
  let component: QrcodesNovoPage;
  let fixture: ComponentFixture<QrcodesNovoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrcodesNovoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrcodesNovoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
