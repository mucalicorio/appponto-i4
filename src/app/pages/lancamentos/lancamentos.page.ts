import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/services/theme/theme.service';
import { WarningService } from 'src/app/services/warning/warning.service';
import { AlertController } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';
import { EntriesService } from 'src/app/services/entries/entries.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { QrcodeService } from 'src/app/services/qrcode/qrcode.service';

const themes = {
  sunny: {
    primary: '#3880ff',
    secondary: '#0cd1e8',
    tertiary: '#7044ff',
    success: '#10dc60',
    warning: '#ffce00',
    danger: '#f04141',
    light: '#f4f5f8',
    medium: '#989aa2',
    dark: '#222428'
  },
  moon: {
    primary: '#8CBA80',
    secondary: '#FCFF6C',
    tertiary: '#FE5F55',
    success: '#10dc60',
    warning: '#ffce00',
    danger: '#f04141',
    medium: '#BCC2C7',
    dark: '#F7F7FF',
    light: '#495867'
  },
  star: {
    primary: '#39BFBD',
    secondary: '#4CE0B3',
    tertiary: '#FF5E79',
    success: '#10dc60',
    warning: '#ffce00',
    danger: '#f04141',
    light: '#F4EDF2',
    medium: '#B682A5',
    dark: '#34162A'
  }
};

@Component({
  selector: 'app-lancamentos',
  templateUrl: './lancamentos.page.html',
  styleUrls: ['./lancamentos.page.scss'],
})
export class LancamentosPage implements OnInit {

  idUser: number;
  entries: any;

  loading = true;

  botaoLancamento = 'entrada';

  entryLocal: any;

  constructor(
    private themeService: ThemeService,
    private alertCtrl: AlertController,
    private userService: UserService,
    private warningService: WarningService,
    private entriesService: EntriesService,
    private companyService: CompanyService,
    private qrcodeService: QrcodeService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.userService.getUserLogged()
      .then((idUser: number) => {
        this.idUser = idUser;
        if (this.idUser === 3) {
          this.getAllEntries();
        } else {
          this.getEntriesUser();
        }
      }, (err) => {
        this.warningService.handleError('Erro ao encontrar usuario', err);
      });

    this.companyService.getCompaniesApi();
    this.qrcodeService.getQrcodesApi();

    this.entriesService.getEntry()
      .then((entry) => {
        console.log('Entry: ', entry);
        this.entryLocal = JSON.parse(entry);

        if (this.entryLocal.saida) {
          this.botaoLancamento = 'sincronizar';
        } else if (this.entryLocal.entrada) {
          this.botaoLancamento = 'saida';
        } else {
          this.botaoLancamento = 'entrada';
        }
      });
  }

  async logout() {
    const alert = await this.alertCtrl.create({
      header: 'Sair',
      message: 'Deseja mesmo sair?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => { }
        }, {
          text: 'Sair',
          handler: () => {
            this.userService.logout();
          }
        }
      ]
    });
    alert.present();
  }

  getAllEntries() {
    return this.entriesService.getAllEntries(this.idUser)
      .subscribe((entries: any) => {
        this.entries = entries.lancamentos;
        console.log('Entries Admin: ', this.entries);
        this.loading = false;
      }, (err) => {
        this.warningService.handleError('Erro ao pegar lancamentos admin.', err);
      });
  }

  getEntriesUser() {
    return this.entriesService.getEntriesUser(this.idUser)
      .subscribe((entries: any) => {
        this.entries = entries.lancamentos;
        console.log('Entries User: ', this.entries);
        this.loading = false;
      }, (err) => {
        this.warningService.handleError('Erro ao pegar lancamentos.', err);
      });
  }

  changeTheme(name) {
    this.themeService.setTheme(themes[name]);
  }

  fakeArrDataLoad(n: number): any[] {
    return Array(n);
  }

  sync() {
    console.log('Entry Local to Sync: ', this.entryLocal);
    this.botaoLancamento = 'carregando';
    this.entriesService.postEntry(this.entryLocal)
      .subscribe((data) => {
        console.log('Sucesso. ', data);
        this.entriesService.removeEntry()
          .then((success) => {
            this.entryLocal = null;
            this.botaoLancamento = 'entrada';

            if (this.idUser === 3) {
              this.getAllEntries();
            } else {
              this.getEntriesUser();
            }
          });
      }, (err) => {
        console.log('Error: ', err);
        this.botaoLancamento = 'sincronizar';
        this.warningService.handleError('Erro ao tentar sincronizar.', err);
      });
  }

}
