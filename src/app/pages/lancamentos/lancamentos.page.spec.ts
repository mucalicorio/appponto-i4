import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LancamentosPage } from './lancamentos.page';

describe('LancamentosPage', () => {
  let component: LancamentosPage;
  let fixture: ComponentFixture<LancamentosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LancamentosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LancamentosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
