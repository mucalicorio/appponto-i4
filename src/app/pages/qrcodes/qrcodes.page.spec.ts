import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrcodesPage } from './qrcodes.page';

describe('QrcodesPage', () => {
  let component: QrcodesPage;
  let fixture: ComponentFixture<QrcodesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrcodesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrcodesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
