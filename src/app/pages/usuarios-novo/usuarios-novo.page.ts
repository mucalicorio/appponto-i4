import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';
import { WarningService } from 'src/app/services/warning/warning.service';

@Component({
  selector: 'app-usuarios-novo',
  templateUrl: './usuarios-novo.page.html',
  styleUrls: ['./usuarios-novo.page.scss'],
})
export class UsuariosNovoPage implements OnInit {

  name: string = null;
  username: string = null;
  password: string = null;
  btnEnabled = true;

  constructor(
    private userService: UserService,
    private router: Router,
    private warningService: WarningService
  ) { }

  ngOnInit() {
  }

  async register(form) {
    this.warningService.showLoading();
    if (form.valid) {
      this.btnEnabled = false;
      await this.userService.register(this.name, this.username, this.password)
        .then(() => {
          form.reset();
          this.btnEnabled = true;
          this.warningService.dismissLoading();
          this.router.navigateByUrl('lancamentos');
        })
        .catch((err: any) => {
          this.warningService.dismissLoading();
          this.btnEnabled = true;
          this.warningService.handleError('Erro ao realizar cadastro.', err.error.error);
        });
    }
  }

}
