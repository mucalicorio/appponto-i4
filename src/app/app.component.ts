import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { UserService } from './services/user/user.service';
import { ThemeService } from './services/theme/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private userService: UserService,
    private themeService: ThemeService
  ) {
    this.initializeApp();
    this.isLogged();
    this.themeService;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.backgroundColorByHexString('#39BFBD');
      this.splashScreen.hide();
    });
  }

  isLogged() {
    this.userService.getAuthorizationToken()
      .then((token: string) => {
        if (token) {
          this.router.navigateByUrl('lancamentos');
        } else {
          this.router.navigateByUrl('login');
        }
      });
  }
}
