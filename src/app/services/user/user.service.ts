import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ApiService } from '../api/api.service';
import { Router } from '@angular/router';
import { CompanyService } from '../company/company.service';
import { QrcodeService } from '../qrcode/qrcode.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  pathUrl = 'usuarios';

  constructor(
    private apiService: ApiService,
    private storage: Storage,
    private router: Router,
    private companyService: CompanyService,
    private qrcodeService: QrcodeService
  ) { }

  async login(username: string, password: string) {
    return await new Promise((resolve, reject) => {
      this.apiService.post(`${this.pathUrl}/login`, { usuario: username, senha: password })
        .subscribe((data: any) => {
          this.setAuthorizationToken(data.token);
          this.setUserLogged(data.id_usuario);
          this.companyService.getCompanies();
          this.qrcodeService.getQrcodes();
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  async register(name: string, username: string, password: string) {
    return await new Promise((resolve, reject) => {
      this.apiService.post(`${this.pathUrl}`, { nome: name, usuario: username, senha: password })
        .subscribe((data: any) => {
          this.setAuthorizationToken(data.token);
          this.setUserLogged(data.id_usuario);
          this.companyService.getCompanies();
          this.qrcodeService.getQrcodes();
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  logout(): void {
    this.storage.remove('token');
    this.storage.remove('idUser');
    this.router.navigateByUrl('login');
  }

  async getAuthorizationToken() {
    return await this.storage.get('token');
  }

  async setAuthorizationToken(token: string) {
    return await this.storage.set('token', token);
  }

  async getUserLogged() {
    return await this.storage.get('idUser');
  }

  async setUserLogged(idUser: number) {
    return await this.storage.set('idUser', idUser);
  }

}
