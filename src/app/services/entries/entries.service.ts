import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class EntriesService {

  pathUrl = 'lancamentos';

  constructor(
    private apiService: ApiService,
    private storage: Storage
  ) { }

  getEntriesUser(idUser: number) {
    return this.apiService.get(`${this.pathUrl}/usuario/${idUser}`);
  }

  getAllEntries(idAdmin: number) {
    if (idAdmin === 3) {
      return this.apiService.get(`${this.pathUrl}`);
    } else {
      console.log('Sai pra lá chulé');
    }
  }

  postEntry(body: any) {
    return this.apiService.post(`${this.pathUrl}`, body);
  }

  async getEntry() {
    return await this.storage.get('entry');
  }

  async setEntry(entry: string) {
    return await this.storage.set('entry', entry);
  }

  removeEntry() {
    return this.storage.remove('entry');
  }

}
