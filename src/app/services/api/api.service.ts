import { Injectable, Inject } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { APP_CONFIG_TOKEN, ApplicationConfig } from 'src/app/app.config';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl: string;

  headers = new HttpHeaders()
    .set('Content-Type', 'application/json; charset=utf-8')
    .set('Accept', 'application/json');

  constructor(
    public httpClient: HttpClient,
    @Inject(APP_CONFIG_TOKEN) private config: ApplicationConfig
  ) {
    this.apiUrl = config.apiUrl;
  }

  get(pathUrl: string) {
    return this.httpClient.get(`${this.apiUrl}/${pathUrl}/`);
  }

  post(pathUrl: string, body: any) {
    return this.httpClient.post(`${this.apiUrl}/${pathUrl}/`, body, { headers: this.headers, responseType: 'json' });
  }

  put(pathUrl: string, body: any) {
    return this.httpClient.put(`${this.apiUrl}/${pathUrl}`, body);
  }

  patch(pathUrl: string, body: any) {
    return this.httpClient.patch(`${this.apiUrl}/${pathUrl}`, body);
  }

  delete(pathUrl: string) {
    return this.httpClient.delete(`${this.apiUrl}/${pathUrl}`);
  }

}
